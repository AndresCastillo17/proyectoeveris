<?php
	require "Generador.php";

	if(isset($_POST['dato'])){
		if($_POST['dato']=='generar'){
			$enlace=mysqli_connect("localhost","root","");
	
			$q="SELECT SCHEMA_NAME
				FROM INFORMATION_SCHEMA.SCHEMATA
				WHERE SCHEMA_NAME = 'proyeveris'";
			if(mysqli_query($enlace,$q)!==''){
				generar();
			}
		}else{
			$resu=[];
			$enlace=mysqli_connect("localhost","root","","proyeveris");
			mysqli_set_charset($enlace, "utf8");
			switch ($_POST['dato']) {

			 	case 'completo':
					//funcion para obtener todas las personas/departamentos
						$query="SELECT *
							FROM DepPersonal
							INNER JOIN personal ON deppersonal.IdPersona = personal.ID
							INNER JOIN departamentos ON deppersonal.IdDep = departamentos.ID
							WHERE deppersonal.IdDep=".$_POST['id']."
							ORDER BY IdPersona DESC;";
						if($row=mysqli_query($GLOBALS["enlace"],$query)){
							while($res=mysqli_fetch_assoc($row)){
								$resu[]=$res;
							}
							echo json_encode($resu);
						}
						mysqli_close($GLOBALS["enlace"]);
					break;

				case 'paises':
					//funcion para conseguir cada pais
						$query="SELECT * FROM paises";
						if($row=mysqli_query($GLOBALS["enlace"],$query)){
							while($res=mysqli_fetch_assoc($row)){
								$resu[]=$res;
							}
							echo json_encode($resu);
						}
						mysqli_close($GLOBALS["enlace"]);
					break;

				case 'coordenadas':
					//funcion  para conseguir coordenadas
						$query="SELECT d.lat,d.lon,d.lugar,p.pais 
							FROM departamentos AS d 
							INNER JOIN paises AS p 
							ON d.PaisID=p.ID";
						if($row=mysqli_query($GLOBALS["enlace"],$query)){
							while($res=mysqli_fetch_assoc($row)){
								$resu[]=$res;
							}
							echo json_encode($resu);
						}
						mysqli_close($GLOBALS["enlace"]);
					break;

				case 'persona':
						$query="SELECT *
							FROM DepPersonal
							INNER JOIN personal ON deppersonal.IdPersona = personal.ID
							INNER JOIN departamentos ON deppersonal.IdDep = departamentos.ID
							WHERE departamentos.lugar='".$_POST['id']."'
							AND FechaBaja IS NULL
							ORDER BY FechaMod DESC
							LIMIT 0,1;";

						if($row=mysqli_query($GLOBALS["enlace"],$query)){
							while($res=mysqli_fetch_assoc($row)){
								$resu[]=$res;
								mysqli_close($GLOBALS["enlace"]);
							}
						}else{
							echo mysqli_error($GLOBALS['enlace']);
						}

						$query="SELECT *
							FROM DepPersonal
							INNER JOIN personal ON deppersonal.IdPersona = personal.ID
							INNER JOIN departamentos ON deppersonal.IdDep = departamentos.ID
							WHERE departamentos.lugar='".$_POST['id']."'
							AND FechaBaja IS NOT NULL
							ORDER BY FechaMod DESC
							LIMIT 0,1;";

						$GLOBALS["enlace"]=mysqli_connect("localhost","root","","proyeveris");
						if($row=mysqli_query($GLOBALS["enlace"],$query)){
							while($res=mysqli_fetch_assoc($row)){
								$resu[]=$res;
								mysqli_close($GLOBALS["enlace"]);
							}
						}else{
							echo "<br>";
							echo mysqli_error($GLOBALS['enlace']);
						}
						echo json_encode($resu);
					break;

				case 'contadorpais':
					$query="SELECT COUNT(*) AS Cant,Pais 
						FROM departamentos 
						INNER JOIN paises ON departamentos.PaisID = paises.ID 
						GROUP BY PaisID";
					if($row=mysqli_query($GLOBALS["enlace"],$query)){
							while($res=mysqli_fetch_assoc($row)){
								$resu[]=$res;
							}
							echo json_encode($resu);
						}
					mysqli_close($GLOBALS["enlace"]);
					break;

				case 'contadorpersona':
					$query="SELECT departamentos.lugar, COUNT(*) AS Cantper
						FROM DepPersonal
						INNER JOIN personal ON deppersonal.IdPersona = personal.ID
						INNER JOIN departamentos ON deppersonal.IdDep = departamentos.ID
						WHERE FechaBaja IS NOT NULL
						GROUP BY IdDep";
					if($row=mysqli_query($GLOBALS["enlace"],$query)){
							while($res=mysqli_fetch_assoc($row)){
								$resu[]=$res;
							}
							echo json_encode($resu);
						}
						mysqli_close($GLOBALS["enlace"]);
					break;

				case 'gen':
					include "Generacion.php";
				break;
				}
			}
		}
?>
