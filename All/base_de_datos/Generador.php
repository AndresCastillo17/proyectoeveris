<?php
	require "Datos.php";
		function generar(){
		//Conexion a servidor
		$link = mysqli_connect('localhost','root','');
		if (!$link){
			die(mysqli_connect_error());
		}

		//Consulta de creaci�n de base de datos
		$query="CREATE DATABASE IF NOT EXISTS ProyEveris";
		if(mysqli_query($link,$query)){
		}
		else{$err.="Error en consulta 1||";}

		//Cierro conexion
		mysqli_close($link);

		//conecto a la BD
		$link= mysqli_connect('localhost','root','','ProyEveris');
		if (!$link){
			die(mysqli_connect_error());
		}

		//Crear tablas
		
		//Tabla de personal
		$query="ALTER DATABASE ProyEveris COLLATE = utf8_spanish_ci;
			CREATE TABLE IF NOT EXISTS personal(
			ID int PRIMARY KEY NOT NULL AUTO_INCREMENT, 
			Nombre varchar(50),
			Apellido varchar(50), 
			Edad int NOT NULL, 
			Sexo varchar(1), 
			FechaMod datetime NOT NULL
		);";
		
		//Tabla de identificacion de paises
		$query.="CREATE TABLE IF NOT EXISTS paises(
			ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
			Pais varchar(60),
			Lon DECIMAL(11,8) NOT NULL,
			Lat DECIMAL(11,8) NOT NULL
		);";

		//Tabla de informacion sobre departamentos
		$query.="CREATE TABLE IF NOT EXISTS departamentos(
			ID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
			PaisID int NOT NULL,
			Lugar varchar(40),
			Lon DECIMAL(11,8) NOT NULL,
			Lat DECIMAL(11,8) NOT NULL,
			FOREIGN KEY (PaisID) REFERENCES Paises(ID)
		);";
		//Tabla de union entre personal y departamentos

		$query.="CREATE TABLE IF NOT EXISTS depPersonal(
			IdPersona int NOT NULL,
			IdDep int NOT NULL,
			FechaAlta date NOT NULL,
			FechaBaja date,
			RazonBaja varchar(50),
			FOREIGN KEY (IdPersona) REFERENCES personal(ID),
			FOREIGN KEY (IdDep) REFERENCES departamentos(ID)
		);";

		//Sube las consultas
		if(mysqli_multi_query($link,$query)){
		}
		else{$err.="Error en consulta 2";}

		//cierra la conexion
		mysqli_close($link);
		
		//llama a la funcion de creacion de datos
		datos();
		if(isset($err)){
			echo $err;
		}
	}
?>
