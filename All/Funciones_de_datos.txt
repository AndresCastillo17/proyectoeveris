Se debe usar $.ajax para llamar y dar datos a funciones.php (nombre a cambiar) en carpeta de base de datos
enviar mediante metodo POST

sintaxis dentro de ajax:
	type:"POST",
	data: { var:'dato',var2:'dato'}

datos posibles:
	variable "dato", define la funcion a usar
	
		valores:
		generar: genera la base de dato ||SI NO EXISTE TAL||
		completo: trae todas las personas y sus datos, practicamente toda la base de datos
		paises: trae todos los paises y sus ids, necesario al traer el dato 'completo'
		coordenadas: trae las coordenadas de todos los departamentos por pais especificado
		persona: trae la ultima persona modificada (ya sea despedida o contratada)
		contadorpais: trae la cantidad de departamentos por pais
		contadorpersona: trae la cantidad de empleados contratados por departamento
	
	en caso de traer los datos 'completo', 'persona', 'ContadorPersona' o 'contadorpais' se debe usar tambien el dato:
		id: ID/    Numero identificador del departamento deseado/    NOMBRE EN STRING de pais deseado para la funcion coordenadas